package inhousemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication
public class InHouseManagementApplication {
	 public static void main(String[] args) {
         SpringApplication.run(InHouseManagementApplication.class, args);
     }
}
