package inhousemanagement.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import inhousemanagement.exception.ResourceNotFoundException;
import inhousemanagement.model.Employee;
import inhousemanagement.service.EmployeeService;

@RestController
public class EmployeeController {

    Logger logger =  LoggerFactory.getLogger(EmployeeController.class);

	 @Autowired
     private EmployeeService employeeService;
	 
	 @RequestMapping("/")
		public String index() {
			return "Hello World!";
	 }
	 //Get all employees data
	 @GetMapping("/employees")
	 public List<Employee> getAllEmployees() {
		    return employeeService.getAllEmployees();
	 }
	 
	 //find employee by Id
	 @GetMapping("/employeesById/{id}")
	 ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId) {
		 Employee employee = employeeService.findByEmployeeId(employeeId)
                 .orElseThrow(()->new ResourceNotFoundException("Employee", "id", employeeId));

		 return ResponseEntity.ok().body(employee);
	             
	 }
	 
	 //find employee by name (query param)
	 @GetMapping("/employeesByName")
	 ResponseEntity<Employee> getEmployeeByName(@RequestParam("first_name") String employeeName) {
		 Employee employee = employeeService.getSpecificEmployees(employeeName)
                 .orElseThrow(()->new ResourceNotFoundException("Employee", "first_name", employeeName));

		 return ResponseEntity.ok().body(employee);
	             
	 }
	 
	// Create a new employee
	 @PostMapping("/saveEmployee")
	 public Employee createEmployee(@RequestBody Employee employee) {
	     return employeeService.saveEmployee(employee);
	 }
	 
	 //Update employee
	 @PutMapping("/updateEmployee/{id}")
     ResponseEntity<Employee> updateEmployee(@PathVariable(value="id") Long id,  @RequestBody Employee new_employee) {
		   Employee employee_obj = employeeService.findByEmployeeId(id)
                                       .orElseThrow(()->new ResourceNotFoundException("Employee", "id", id));
		   employee_obj.setId(new_employee.getId());
		   employee_obj.setFirst_name(new_employee.getFirst_name());
		   employee_obj = employeeService.saveEmployee(employee_obj);
           return ResponseEntity.ok().body(employee_obj);    
       }
	 //Delete employee
   @DeleteMapping("/deleteEmployee/{id}")
   ResponseEntity deleteEmployee(@PathVariable(value="id") Long id){
	   Employee employee = employeeService.findByEmployeeId(id).orElseThrow(()->new ResourceNotFoundException("Employee", "id", id));
	   employeeService.deleteEmployee(employee.getId());
	   logger.info("Delete Successfull");
	   return ResponseEntity.ok().body("Delete Success");
   }
}
