package inhousemanagement.service;

import java.util.List;
import java.util.Optional;

import inhousemanagement.model.Employee;


public interface EmployeeService {

	List<Employee> getAllEmployees();
	
	Optional<Employee> findByEmployeeId(long id);
	
	Employee saveEmployee(Employee employee);
	
	void deleteEmployee(long id);
	
	Optional<Employee> getSpecificEmployees(String first_name);
}
