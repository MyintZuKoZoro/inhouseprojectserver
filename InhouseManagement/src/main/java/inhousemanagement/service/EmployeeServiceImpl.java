package inhousemanagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import inhousemanagement.model.Employee;
import inhousemanagement.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository employeeRepo;
	
	@Override
	public List<Employee> getAllEmployees() {
		// do some business processing here ...
		return employeeRepo.findAll();
	}

	@Override
	public Optional<Employee> findByEmployeeId(long id) {
		// do some business processing here ...
		return employeeRepo.findById(id);
	}

	@Override
	public Employee saveEmployee(Employee employee) {
		// do some business processing here ...
		return employeeRepo.save(employee);
	}

	@Override
	public void deleteEmployee(long id) {
		// do some business processing here ...
		employeeRepo.deleteById(id);
	}

	@Override
	public Optional<Employee> getSpecificEmployees(String first_name) {
		// TODO Auto-generated method stub
		return employeeRepo.findSpecificUser(first_name);
	}

}
