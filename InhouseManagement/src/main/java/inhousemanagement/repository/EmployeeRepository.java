package inhousemanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import inhousemanagement.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	@Query("SELECT u FROM Employee u WHERE u.first_name = :first_name")
	Optional<Employee> findSpecificUser(@Param("first_name") String first_name);

}
